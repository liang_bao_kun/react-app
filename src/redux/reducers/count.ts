/**
 * @author: LBK
 * @date: 2024/1/24
 * @description：定义修改数据的方法
 * 函数接收两个参数，分别为之前的状态（preState默认定义为0，否则初始值没有的话默认为undefined）动作对象（action包含type 和data）
 */
export default function countReducer(
    preState = 0,
    action: { type: string; data: any }
): number {
    const {type, data} = action
    // 根据type进行数据操作
    switch (type) {
        case 'increment':
            return preState + data
        case 'decrement':
            return preState - data
        default:
            return preState
    }
}
