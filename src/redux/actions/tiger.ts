/**
 * @author: LBK
 * @date: 2024/1/24 23:01
 * @description：tiger.ts
 */

import { TigerVo } from "../reducers/tiger"
export const increTiger = (data: TigerVo) => ({ type: 'add', data })
export const delTigerById = (data: TigerVo) => ({ type: 'del', data })
