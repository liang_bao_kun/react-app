// import moment from 'moment';
// import { requestNoReply, openDevice } from '../api/api';
// import { pickerGenerate } from './constant';
// import { sortBy, toNumber } from 'lodash';
// import I18n from '../i18n';
//
// /**
//  * 获取当前时间
//  * @returns {String}
//  */
// export const now = () => {
//   return moment().format('YYYY.MM.DD HH:mm:ss.SSS');
// };
// //温度初始化遍历
// export const tempInit = (min: number, max: number, step: number) => {
//   let _arr: Array<string> = [];
//   for (let i = min; i <= max; i = i + step) {
//     _arr.push(i + '');
//   }
//   return _arr;
// };
//
// //摄氏度转华氏度
// export const CTOF = (val: String) => {
//   val = (+val * 9) / 5 + 32 + '';
//   return val;
// };
//
// //华氏度转摄氏度
// export const FTOC = (val: String) => {
//   val = ((+val - 32) * 5) / 9 + '';
//   return val;
// };

/**
 * 过滤出指定关键字的信息
 * @returns {String}
 */
export const filterTag = (list: any[], name: string, value: string) => {
  const _list: any =
    list.length > 0 && list.filter(item => item[name] === value);
  return _list;
};

// /**
//  * 发送控制指令
//  * @param commands 指令集合
//  * @param commandsType 指令类型集合
//  * @param controlCallback 延迟刷新函数
//  * @returns
//  */
// export const sendDeviceCommands = (
//   commands: any,
//   commandsType: any,
//   controlCallback: Function,
// ) => {
//   // console.log(
//   //   `${now()}: sendDeviceCommands -> commands: ${JSON.stringify(commands)}`,
//   // );
//
//   if (Object.keys(commands).length === 0) {
//     return;
//   }
//   //仅发送指令时根据配置类型转化
//   let newObj: any = {};
//   Object.keys(commands).forEach(_v => {
//     if (!commandsType[_v]) {
//       //参数未配置时指令需要时 默认值
//       newObj[_v] = commands[_v];
//     } else {
//       const _type = commandsType[_v].type;
//       if (_type === 'BOOL') {
//         newObj[_v] = ToBoolen(commands[_v]);
//       } else if (_type === 'ENUM' || _type === 'NUM' || _type === 'FAULT') {
//         newObj[_v] = toNumber(commands[_v]);
//       } else {
//         newObj[_v] = commands[_v];
//       }
//     }
//   });
//   console.log(
//     `${now()}: sendDeviceCommands -> commands: ${JSON.stringify(newObj)}`,
//   );
//
//   controlCallback && controlCallback();
//
//   const params = JSON.stringify({
//     instruction: newObj,
//   });
//
//   requestNoReply({
//     params,
//     success: () => {},
//     fail: () => {},
//   });
// };
//
// /**
//  * 发送开机控制指令 单独针对热水器
//  * @param controlCallback 延迟刷新函数
//  * @returns
//  */
// export const sendDeviceON = (controlCallback: Function) => {
//   console.log(`${now()}: sendDeviceON-> commands:`);
//
//   controlCallback && controlCallback();
//
//   openDevice({
//     success: () => {},
//     fail: () => {},
//   });
// };
//
// //换算成可供显示的周*等格式
// export const weekdaysFn = (arr: any) => {
//   const weekNames = [
//     'period.Mon',
//     'period.Tues',
//     'period.Wed',
//     'period.Thur',
//     'period.Fri',
//     'period.Sat',
//     'period.Sun',
//   ];
//   let name: any;
//   // arr = arr.sort(); //排序
//   arr = sortBy(arr);
//   if (arr.length > 0) {
//     if (arr + '' === [1, 2, 3, 4, 5, 6, 7] + '') {
//       name = I18n.t('period.everyday');
//     } else if (arr + '' === [1, 2, 3, 4, 5] + '') {
//       name = I18n.t('period.work_day');
//     } else if (arr + '' === [6, 7] + '') {
//       name = I18n.t('period.weekend');
//     } else {
//       name = [];
//       arr.forEach((_item: number) => {
//         const _index: any = String(_item - 1);
//         name.push(I18n.t(weekNames[_index]));
//       });
//       name = name.join('、');
//     }
//   } else {
//     name = I18n.t('period.only_once');
//   }
//   return name;
// };
//
// // 获取时间picker默认显示的数据
// export const getModalDefault = (initValue: any) => {
//   const hour = initValue.split(':')[0];
//   const minute = initValue.split(':')[1];
//   const picker = pickerGenerate(hour, minute);
//   let default_arr: any[] = [];
//   picker.map(i => {
//     if (i.unit) {
//       default_arr.push(i.unit);
//     } else {
//       default_arr.push(i.default);
//     }
//   });
//   return default_arr;
// };

//比较结束时间是否小于开始时间
export const compareTimeTomorrow = (start: string, end: string) => {
  const date = new Date();
  const a: any = start.split(':');
  const b: any = end.split(':');
  return (
    date.setHours(a[0], a[1]) > date.setHours(b[0], b[1]) ||
    date.setHours(a[0], a[1]) === date.setHours(b[0], b[1])
  );
};

// export const compareTime = (start: string, end: string) => {
//   //时间相差不能大于21:59
//   // const date = new Date();
//   const a: any = start.split(':');
//   const b: any = end.split(':');
//   const startMin = a[0] * 60 + Number(a[1]);
//   const endMin = b[0] * 60 + Number(b[1]);
//   const comparMin = 21 * 60 + 59;
//   return endMin > startMin
//     ? endMin - startMin > comparMin
//     : endMin + 24 * 60 - startMin > comparMin;
// };
// // 转换为picker的格式
// export const getSettingData = (initValue: any, UNIT?: string) => {
//   let pickerArr: any = [];
//   const hour = initValue.split(':')[0];
//   const minute = initValue.split(':')[1];
//   const picker = pickerGenerate(hour, minute, UNIT);
//   picker.map(item => {
//     let arr = [];
//     const step = item.step ? item.step : 1;
//     if (item.default && item.high) {
//       for (let i = item.low; i <= item.high; i = i + step) {
//         const label = i <= 9 ? '0' + i : i;
//         const value = i <= 9 ? '0' + i : i;
//         arr.push({
//           label: label + '',
//           value: value + '',
//         });
//       }
//     } else {
//       arr.push({
//         label: item.unit,
//         value: item.unit,
//       });
//     }
//
//     pickerArr.push(arr);
//   });
//   return pickerArr;
// };
//
// // 转换为布尔类型
// export const ToBoolen = (_v: any) => {
//   let _n = Boolean(+_v);
//   return _n;
// };
