// /* 测试Radium导语法 */
// import React, {Component} from "react";
// import Radium from 'radium';
//
// let styles = {
//     base: {color: '#fff', ':hover': {background: '#0074d9'}},
//     primary: {background: '#0074D9'},
//     warning: {background: '#FF4136'}
// };
//
// class Test extends Component {
//     constructor(props, context) {
//         super(props);
//     }
//
//     render() {
//         return (<div>
//             <button style={[styles.base, styles.primary]}> this is a primary button</button>
//         </div>);
//     }
// }
// export default Radium(Test)

export default function About() {
    return (
        <div>
            测试Radium导语法
        </div>
    )
}
