import type {Dayjs} from 'dayjs';
import React from 'react';
import {Calendar, theme} from 'antd';
import type {CalendarProps} from 'antd';

const onPanelChange = (value: Dayjs, mode: CalendarProps<Dayjs>['mode']) => {
    console.log(value.format('YYYY-MM-DD'), mode);
};

const App: React.FC = () => {
    const {token} = theme.useToken();

    const wrapperStyle: React.CSSProperties = {
        width: 300,
        border: `1px solid ${token.colorBorderSecondary}`,
        borderRadius: token.borderRadiusLG,
        backgroundColor: 'red',
    };

    let styles = {
        base: {color: '#fff', ':hover': {background: '#0074d9'}},
        primary: {background: '#0074D9'},
        warning: {background: '#FF4136', height: '30px'}
    };

    return (
        <div>
            <div style={wrapperStyle}>
                <Calendar fullscreen={false} onPanelChange={onPanelChange}/>
            </div>
            <div style={styles.warning}>
                测试高度
            </div>
            {/*<div style={[styles.warning]}>*/}
            {/*    数值方式引用css*/}
            {/*</div>*/}
        </div>
    );
};

export default App;