import React, { useState, useEffect } from 'react';

export default function useEffects() {
    const [num, setNum] = useState(1)
    /**
     * 第一个参数是回调函数
     * 第二个参数是依赖项
     * 每次num变化时都会变化
     *
     * 注意初始化的时候，也会调用一次
     */
    useEffect(() => {
        console.log("每次num，改变我才会触发")
    }, [num])
    return (
        <div>
            <button onClick={() => setNum(num + 1)}>+1</button>
             <div>你好，react hook{num}</div>
        </div>
    );
}
