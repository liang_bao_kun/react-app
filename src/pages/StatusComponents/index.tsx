/*在组件间共享状态： */
/*有时候你希望两个组件的状态始终同步更改。要实现这一点，可以将相关状态从这两个组件上移除，并把这些状态移到最近的父级组件，
然后通过 props 将状态传递给这两个组件。这被称为“状态提升”，这是编写 React 代码时常做的事。
在以下示例中，要求每次只能激活一个面板。要实现这一点，父组件将管理激活状态并为其子组件指定 prop，而不是将激活状态保留在各自的子组件中。*/
import {useState} from 'react';

export default function Accordion() {
    const [activeIndex, setActiveIndex] = useState(0);
    return (
        <div>2222</div>
        // <>
        //     <h2>Almaty, Kazakhstan</h2>
        //     <Panel
        //         title="关于"
        //         isActive={activeIndex === 0}
        //         onShow={() => setActiveIndex(0)}>
        //         阿拉木图人口约200万，是哈萨克斯坦最大的城市。在1929年至1997年之间，它是该国首都。
        //     </Panel>
        //     <Panel
        //         title="词源"
        //         isActive={activeIndex === 1}
        //         onShow={() => setActiveIndex(1)}>
        //         这个名字源于哈萨克语 <span lang="kk-KZ">алма</span>，是“苹果”的意思，通常被翻译成“满是苹果”。事实上，阿拉木图周围的地区被认为是苹果的祖籍，<i
        //         lang="la">Malus sieversii</i> 被认为是目前本土苹果的祖先。
        //     </Panel>
        // </>
    );
}

// @ts-ignore
/*组件调用*/
// function Panel({title, isActive, onShow}) {  //形参标识类型，未使用形参children
function Panel() {  //形参标识类型，未使用形参children
    return (
        // <section className="panel">
        //     <h3>{title}</h3>
        //     {isActive ? (
        //         <p>333333</p>
        //     ) : (
        //         <button onClick={onShow}>
        //             显示
        //         </button>
        //     )}
        // </section>
        <button>
            显示
        </button>
    );
}
