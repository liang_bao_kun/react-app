/* redux测试页面*/

import Count from '@/containers/Count/index'
import Tiger from '@/containers/Tiger/index'

function testIndex() {
    return (
        <div className="redux">
            <div style={{margin: '50px 0'}}></div>
            <Count/>
            <div style={{margin: '50px 0'}}></div>
            <hr/>
            <Tiger/>
        </div>
    );
}

export default testIndex;
