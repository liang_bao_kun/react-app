import {Navigate} from 'react-router-dom';
import About from '@/pages/About';
import Homes from '@/pages/Home';
import Calendar from '@/pages/Calendar'; //日期选择组件
import UseEffects from '@/pages/useEffects'; //日期选择组件
import StatusComponents from '@/pages/StatusComponents'; //组件间共享状态（相同父级组件）
import ReduxPage from '@/pages/ReduxPage'; //redux测试页面

export default [
    {
        path: '/about',
        element: <About />,
    },
    {
        path: '/home',
        element: <Homes />,
    },
    /* css测试 */
    {
        path: '/calendar',
        element: <Calendar />,
    },
    /* 组件间共享状态（相同父级组件） */
    {
        path: '/statusComponents',
        element: <StatusComponents />,
    },
    /* 变量监听 useEffects */
    {
        path: '/useEffects',
        element: <UseEffects />,
    },
    /* redux测试 */
    {
        path: '/reduxTest',
        element: <ReduxPage />,
    },
    {
        // 路由重定向
        path: '/',
        element: <Navigate to='/home' />,
    },
] as {
    path: string
    element: JSX.Element
}[]
