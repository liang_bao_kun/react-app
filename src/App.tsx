import React, {useState} from 'react'
import { NotificationOutlined, UserOutlined,} from '@ant-design/icons'
import type {MenuProps} from 'antd'
import {Layout, Menu, theme} from 'antd'
import {useNavigate, useRoutes} from 'react-router-dom'
import routes from './routes/index'

const {Header, Content, Sider} = Layout

const titleMenu: MenuProps['items'] = ['1', '2', '3'].map((key) => ({
    key,
    label: `标题 ${key}`,
}))

const siderMenu: MenuProps['items'] = [
    {
        key: 'home',
        icon: <UserOutlined/>,
        label: '人员管理',
    },
    {
        key: 'about',
        icon: <NotificationOutlined/>,
        label: '关于demo',
    },
    {
        key: 'calendar',
        icon: <NotificationOutlined/>,
        label: 'css组件样式',
    },
    {
        key: 'statusComponents',
        icon: <NotificationOutlined/>,
        label: '组件间共享状态（相同父级组件）',
    },
    {
        key: 'useEffects',
        icon: <NotificationOutlined/>,
        label: '变量监听 useEffects',
    },
    {
        key: 'reduxTest',
        icon: <NotificationOutlined/>,
        label: 'reduxTest',
    },
    /* {
      key: 'info',
      icon: <LaptopOutlined/>,
      label: '菜单管理',
      children: [
          {
              key: 'info-detail',
              label: '信息详情',
          },
          {
              key: 'info-look',
              label: '信息查询',
          },
      ],
  },
  {
      key: 'statistics',
      icon: <NotificationOutlined/>,
      label: '测试统计',
  },*/
]

const App: React.FC = () => {
    const {token: {colorBgContainer}} = theme.useToken()
    // 获得路由表
    const routeView = useRoutes(routes)
    const navigate = useNavigate()
    // 面包屑名称
    const [breadcrumbName, setBreadcrumbName] = useState('home')
    // 点击菜单
    const handleSiderClick: MenuProps['onClick'] = ({key, keyPath}) => {
        const name = keyPath.reverse().join('/') || ''
        setBreadcrumbName(name)
        // 路由跳转
        navigate(key, {
            replace: false,
            state: {
                id: key,
            },
        })
    }

    return (
        <Layout>
            <Header className='header'>
                <div className='logo'/>
                <Menu
                    theme='dark'
                    mode='horizontal'
                    defaultSelectedKeys={['1']}
                    items={titleMenu}
                />
            </Header>
            <Layout>
                {/*左侧布局*/}
                <Sider width={200} style={{background: colorBgContainer, height: 'calc( 100vh - 64px)'}}>
                    <Menu
                        mode='inline'
                        defaultSelectedKeys={['1']}
                        defaultOpenKeys={['sub1']}
                        style={{height: '100%', borderRight: 0}}
                        items={siderMenu}
                        onClick={handleSiderClick}
                    />
                </Sider>
                {/*右侧布局*/}
                <Layout style={{padding: '0 24px 24px'}}>
                    <div style={{margin: '16px 0'}}>{breadcrumbName}</div>
                    <Content style={{padding: 24, margin: 0, minHeight: 280, background: colorBgContainer,}}>
                        {routeView}
                    </Content>
                </Layout>
            </Layout>
        </Layout>
    )
}

export default App

