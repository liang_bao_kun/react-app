import React from 'react';
import ReactDOM from 'react-dom/client';
import './assets/css/index.css';
import App from './App'; //导入项目根组件
import {BrowserRouter} from 'react-router-dom'
import {ConfigProvider} from 'antd';
//语言汉化
import zhCN from 'antd/locale/zh_CN';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn'
import store from './redux/store'
import { Provider } from 'react-redux'
dayjs.locale('zh-cn')


const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement  //渲染到节点
);
root.render(
        <BrowserRouter>
            <ConfigProvider locale={zhCN}>
                <Provider store={store}>
                    <App />
                </Provider>
            </ConfigProvider>
        </BrowserRouter>
);
